
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>@yield('title',"Hôpital d’Instruction des Armées-Centre Hospitalier Universitaire de Parakou (+229 23 61 19 00)") | HIA Parakou, Bénin</title>
        <meta charset="utf-8">
        <link rel="SHORTCUT ICON" href="{{asset('favicon.ico') }}" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="author" content="ETEKA S. Wilfried" />
        <meta name="revisit-after" content="15" />
        <meta name="language" content="fr" />
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="identifier-url" content="http://www.univ-parakou.bj/" />
        <meta name="keywords" content="Hôpital, Centre Hospitalier, Centre Universitaire, HIA-CHU, Parakou, Bénin Hôpital, Health, Healthcare, département du Borgou, Clinic, Care, Hospital, HIA, Forces Armées,Hôpital d'Instruction"> 
        <?php $desc = "HIA - L'Hôpital d'Instruction des Armées-Centre Hospitalier Universitaire (HIA-CHU) de Parakou est situé à Parakou dans le département du Borgou, au quartier Sinagourou. E-mail : hiachu2@gmail.com | Tél : +229 23 61 19 00/23 61 38 88"; ?>
        <meta name="Description" content="@yield('description',$desc)"/> 
        

    
        <!-- Plugins CSS -->
        <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">     
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <!-- BOOTSTRAP CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
                
        <!-- FONT ICONS -->
        <link href="../../../../use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" rel="stylesheet" crossorigin="anonymous">      
        <link href="css/flaticon.css" rel="stylesheet">

        <!-- PLUGINS STYLESHEET -->
        <link href="css/menu.css" rel="stylesheet"> 
        <link id="effect" href="css/dropdown-effects/fade-down.css" media="all" rel="stylesheet">
        <link href="css/magnific-popup.css" rel="stylesheet">   
        <link href="css/owl.carousel.min.css" rel="stylesheet">
        <link href="css/owl.theme.default.min.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/jquery.datetimepicker.min.css" rel="stylesheet">
                
        <!-- TEMPLATE CSS -->
        <link href="css/style.css" rel="stylesheet">
         <style type="text/css">
            .wsmenu-list>li>a{
            //text-transform: uppercase;
            font-size: 13px;
            padding-right: 10px;
            }

            .yellow-color{
                color:#f4ee2d;
            }
            .hmin-500{
                min-height: 500px;
            }
         @media(min-width:768px){ 
            
             #header-2 .wsmenu > .wsmenu-list > li > a{
                padding: 10px 5px ;
            }
           #header-2 .wsmainfull.cloned .wsmenu > .wsmenu-list > li > a, #header-2 .wsmenu > .wsmenu-list > li.dropmenu > a{
                padding: 10px 30px 10px 2px !important
            }
            @media (max-width: 1920.99px) and (min-width: 1440px){
                 .slider,.slider .slides
                {
                    height: 450px !important;
                }
                 #hero-7 h2{
                    font-size: 2rem;
                 }
                 #hero-7 .caption p{
                    font-size: 14px;
                    color: #333333;
                 }
            }
           
         /*   #page {
            width: 75%;
            background: #ffffff;
            margin: auto;
            margin-top: 80px;
            box-shadow: 0 0 5px #ddd;
            border-radius: 15px;
            margin-bottom: 150px;
        }*/
        .hero-widget{
            background: #ffffff;
            border-bottom: 0px solid #f4ee2d;
            border-top: 3px solid #f4ee2d;
        }
        //.container{width: 80% !important}
        }
        ul.wsmenu-list li a{
            font-weight: 800
        }
        body{
            background: url('assets/images/bg_hia.png') #eee;
            font-family:'Source Sans Pro', 'Segoe UI', sans-serif;
            font-family: "Lato", sans-serif;
            position: relative;
            //font-family: "Roboto", sans-serif;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #1b1e24;
            text-align: left;
}
#hia_logo{
    border: 5px solid #fff;
    border-radius: 5px;
    height: 50px

}
.hero-widget {
    background: #2a5d68;
}
body {
    margin: 0;
    
    background-color: #fafbff;
        }
        </style>
        
        <!-- RESPONSIVE CSS -->
        <link href="css/responsive.css" rel="stylesheet"> 
        @yield('css')
       
    </head>
    <body class="bge">

        @section('nav')
        
   
        <!-- PRELOADER SPINNER
        ============================================= -->   
        <div id="loader-wrapper">
            <div id="loader"><div class="loader-inner"></div></div>
        </div>
        <!-- PAGE CONTENT
        ============================================= -->   
        <div id="page" class="page">

            <!-- HEADER
            ============================================= -->
            <header id="header-2" class="header ">


                <!-- MOBILE HEADER -->
                <div class="wsmobileheader clearfix">
                    <a id="wsnavtoggle" class="wsanimated-arrow"><span></span></a>
                    <span class="smllogo"><img src="{{asset('assets/images/logo-hia.png')}}" width="180" height="40" alt="mobile-logo"/></span>
                    <!--a href="tel:123456789" class="callusbtn"><i class="fas fa-phone"></i></a-->
                </div>


                <!-- HEADER WIDGETS -->
                <div class="hero-widget clearfix">
                    <div class="container">
                        <div class="row d-flex align-items-center">


                            <!-- LOGO IMAGE -->
                            <div class="col-md-5 col-xl-6">
                                <div class="desktoplogo"><a href="#hero-7"><img id="hia_logo" src="{{asset('assets/images/logo-hia.png')}}"   alt="header-logo"></a></div>
                            </div>
                            <!-- WIDGETS -->
                            <div class="col-md-7 col-xl-6">
                                <div class="row">
                                    <!-- Emergency Cases Widget-- >
                                    <div class="col-md-6">
                                        <div class="header-widget icon-xs">
                                            <span class="flaticon-039-emergency-call-1 blue-color"></span>
                                            <div class="header-widget-txt">
                                                <p>Emergency Cases</p> 
                                                <p class="header-widget-phone steelblue-color">1-800-123-4560</p>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Working Hours Widget -->
                                    <div class="col-md-6">
                                        <div class="header-widget icon-xs">
                                            <span class="flaticon-092-clock yellow-color"></span>
                                            <div class="header-widget-txt">
                                                <p class="txt-400 text-white">Lundi – Dimanche : - 24H/24 </p>
                                                <p class="lightgrey-color">Ouvert 7 Jours / 7</p>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Location Widget -->
                                    <div class="col-md-6">
                                        <div class="header-widget icon-xs">
                                            <span class="flaticon-021-hospital-9 yellow-color"></span>
                                            <div class="header-widget-txt">
                                                <p class="txt-400 text-white">Parakou, Okédama</p> 
                                                <p class="lightgrey-color">hiachu2@gmail.com</p>
                                            </div>
                                        </div>
                                    </div> 

                                </div>
                            </div>  <!-- END WIDGETS -->

                        </div>
                    </div>
                </div>  <!-- END HEADER WIDGETS -->


                <!-- NAVIGATION MENU -->
                <div class="wsmainfull menu clearfix">
                    <div class="wsmainwp clearfix">

                        <!-- LOGO IMAGE -->
                        <!-- For Retina Ready displays take a image with double the amount of pixels that your image will be displayed (e.g 360 x 80 pixels) -->
                        <div class="desktoplogo"><a href="#hero-7"><img id="hia_logo" src="{{asset('assets/images/logo-hia.png')}}"  alt="header-logo"></a></div>

                        <!-- MAIN MENU -->
                        <nav class="wsmenu clearfix">
                            <ul class="wsmenu-list">


                                  <!-- DROPDOWN MENU -->
                                <li id="accueil" ><a class="active" href="{{url('/')}}">Accueil </a>
                                    
                                </li>   <!-- END DROPDOWN MENU -->

                                <li aria-haspopup="true" class="dropmenu"><a href="#">L'Hôpital <span class="wsarrow"></span></a>
                                    <ul class="sub-menu">
                                        <li aria-haspopup="true">
                                            <a href="{{route('web.presentation')}}"><i class="ti-plus"> </i>Présentation</a>
                                        </li>
                                        <li aria-haspopup="true">
                                            <a href="{{route('web.direction_generale')}}">La direction Générale</a>
                                        </li>
                                        <li aria-haspopup="true">
                                            <a href="{{route('web.projet_rapport')}}">Etudes projets et Rapports</a>
                                        </li>
                                        <li aria-haspopup="true">
                                            <a href="{{route('web.partenariat_jumelage')}}">Partenariats et Jumélage</a>
                                        </li>
                                        <li aria-haspopup="true">
                                            <a href="{{route('web.oraganisation_travaux')}}">Organisation des travaux</a>
                                        </li>
                                        
                                    </ul>
                                </li>  
                                <li aria-haspopup="true"><a href="{{route('web.services_medico_tech')}}">Services Médico-Techniques </a>
                                </li>  

                                <li aria-haspopup="true"><a href="{{route('web.services_admin_fin')}}">Services Administratifs et Finances </a>
                                </li>

                                 
                                
                                <li aria-haspopup="true" class="dropmenu"><a href="#">Infos utiles <span class="wsarrow"></span></a>
                                    <div class="wsmegamenu clearfix halfmenu">
                                        <div class="container-fluid">
                                            <div class="row">

                                                <!-- Links -->
                                                <ul class="col-lg-6 col-md-12 col-xs-12 link-list">
                                                    <li class="title">Pour les Patients:</li>
                                                    <li><a href="#">Prise de rendez-vous</a></li>                                               
                                                    <li><a href="#">Carnet du patient</a></li>
                                                    <li><a href="#">Services d'urgences</a></li>
                                                    <li><a href="#">Témoignages des patients Testimonials</a></li>
                                                    <li><a href="#">Dernières nouvelles</a></li>                                                 
                                                </ul>

                                                <!-- Links -->
                                                <ul class="col-lg-6 col-md-12 col-xs-12 link-list">
                                                    <li class="title">Liens utiles:</li>
                                                    <li><a href="#">Conditions d'accès</a></li>
                                                    <li><a href="#">Prise en charge des malades</a></li>
                                                    <li><a href="#">Travailler à l'HIA</a></li>
                                                    <li><a href="#">Information sur l'assurance</a></li>
                                                    <li><a href="#">Après le traitement des patients</a></li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li aria-haspopup="true" class="dropmenu"><a href="#">Infos PRMP <span class="wsarrow"></span></a>
                                    <ul class="sub-menu">
                                        <li aria-haspopup="true">
                                            <a href="{{route('web.demande_cotisation')}}">Demandes de cotation</a>
                                        </li>
                                        <li aria-haspopup="true">
                                            <a href="{{route('web.avis_attribution')}}">Avis d'attributions</a>
                                        </li>
                                    </ul>
                                </li>   
                                <li ><a href="{{route('web.contact')}}">Contact </a></li>
                                <!-- MEGAMENU -->
                                

                                <!-- HIDDEN NAVIGATION MENU BUTTON -->
                                <!--li class="nl-simple header-btn" aria-haspopup="true"><a class="blue-hover" href="timetable.html">Our Doctors Timetable</a></li--> 


                            </ul>

                        </nav>  <!-- END MAIN MENU -->


                        <!-- NAVIGATION MENU BUTTON -->
                        <!--div class="header-button">
                            <span class="nl-simple header-btn blue-hover"><a href="{{url('/')}}">Liste de nos médécins</a></span>
                        </div-->

                    </div>
                </div>  <!-- END NAVIGATION MENU -->


            </header>   <!-- END HEADER -->


        @show
        @yield('content')

        <!-- EXTERNAL SCRIPTS
        ============================================= -->   
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script> 
        <script src="js/modernizr.cusstom.js"></script>
        <script src="js/jquery.easing.js"></script>
        <script src="js/jquery.appear.js"></script>
        <script src="js/jquery.stellar.min.js"></script>    
        <script src="js/menu.js"></script>
        <script src="js/sticky.js"></script>
        <script src="js/jquery.scrollto.js"></script>
        <script src="js/materialize.js"></script>   
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script> 
        <script src="js/imagesloaded.pkgd.min.js"></script>
        <script src="js/isotope.pkgd.min.js"></script>
        <script src="js/hero-form.js"></script>
        <script src="js/contact-form.js"></script>
        <script src="js/comment-form.js"></script>
        <script src="js/appointment-form.js"></script>
        <script src="js/jquery.datetimepicker.full.js"></script>        
        <script src="js/jquery.validate.min.js"></script>   
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <!--script src="js/wow.js"></script-->   
    
        <!-- Custom Script -->      
        <script src="js/custom.js"></script>

        <script> 
          // new WOW().init();
        </script>

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
        <!-- [if lt IE 9]>
            <script src="js/html5shiv.js" type="text/javascript"></script>
            <script src="js/respond.min.js" type="text/javascript"></script>
        <![endif] -->

        <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. --> 
        <!--
        <script>
            window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
            ga('create', 'UA-XXXXX-Y', 'auto');
            ga('send', 'pageview');
        </script>
        <script async src='https://www.google-analytics.com/analytics.js'></script>
        -->
        <!-- End Google Analytics -->

        <script src="js/changer.js"></script>
        <script defer src="js/styleswitch.js"></script> 
        

        @yield('script')
    </body>
</html>
