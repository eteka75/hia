

<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
		 <title>@yield('title',"Administration") | FANEPIA </title>
        <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		 <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="author" content="ETEKA S. Wilfried" />

		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.png" type="image/x-icon">

		<!--  Social tags -->
		<meta name="keywords" content="Application Web, SFD, Promoteur, PME, Banque, Prêt , Free, Finance">
		<meta name="description" content="... est une Application de Gestion de prêt ">

		<!-- Schema.org -->
		<meta itemprop="name" content="Application de ...">
		<meta itemprop="description" content="Application de gestion de solvabilité de Prêt">
		<meta itemprop="image" content="assets/img/image-app.png">

		<!-- Google Fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

		<!-- CSS Implementing Plugins Icones-->
		<!--link rel="stylesheet" href="{{asset('assets/plugins/font-awesome/css/fontawesome-all.min.css')}}"-->
		<link rel="stylesheet" href="{{asset('assets/plugins/themify-icons/themify-icons.css')}}">

		<!--link rel="stylesheet" href="{{asset('assets/plugins/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css')}}"-->
		<!--link rel="stylesheet" href="{{asset('assets/plugins/jquery-ui/themes/base/jquery-ui.min.css')}}"-->
		<!--link rel="stylesheet" href="{{asset('assets/plugins/prism/prism.css')}}"-->

		<!-- CSS Template -->
		<link rel="stylesheet" href="{{asset('assets/css/theme.css')}}">
		<link rel="stylesheet" href="{{asset('assets/css/layout.css')}}">
		<style type="text/css">
			
			.hidden{display: none}
			.rond5{border-radius: 5px;}
			.card{
				box-shadow: 0 .125rem .25rem 0 rgba(143, 143, 143, 0.64);
				border-radius: 15px;
			}
			.bg-dark2{
				background: #f3f4ff !important
			}
			fieldset{
				border: 1px solid #dddddd;
				border-radius: 5px;
				padding: auto;
			}
			.text-dark{
				color: #e5fdf4 !important;
				//text-shadow: 0 1px #282222;
			}
			a, .text-primary{
				//color: #784a24 !important;
			}
			.u-footer{
				 background: #c9f2e9;
  				color: #000000;
  				border-top-width: 0px;
			}
			fieldset legend{
				width: auto;
				margin: auto 2%;
				font-weight: bold;
			}
			.bg-main-log {
			    background: #2a5d68;
			}
			.hia_logo{
				max-width: 14rem;
				border-radius: 5px;
				border: 5px solid #ffffff;
			}
			.brd2card{
				border: 2px solid #22d3b0
			}
			.brd1card{
				border: 1px solid #22d3b0
			}
/*.bg-main-log{
	background: url('{{asset('assets/img/lr-bg.png')}}') #98c54e;
}*/
</style>
</head>
<body class="bg-white">


  <main class="d-flexs flex-column u-hero u-hero--end mnh-100vh bg-main-log" style="">
  	<div>	
  		<div class="container-fluid">	
	  		<div class="row">	
		  		<div class="col-md-12">	
		 		<div class="text-center mt-4">
		            <a href="{{url('/')}}"><img align="HIA-PArakou" src="{{asset('assets/images/logo-hia.png')}}"  class="hia_logo" ></a>
		            </div>
		  		</div>
	  		</div>
  		</div>
           
        @yield('content')

      <!-- Footer -->
      <footer class="u-footer mt-auto">
          <div class="container">
              <div class="d-md-flex_ align-items-md-center_ text-center text-md-center_ ">
                  <!-- End Footer Menu -->

                  <!-- Copyright -->
                  <span class=" ml-auto_">© {{date('Y')}} <b>HIA -  Centre Hospitalier de Parakou,</b> Tout droit réservé.</span>
                  <!-- End Copyright -->
              </div>
          </div>
      </footer>
      </div>
      <!-- End Footer -->
  </main>
  <!-- End Go to Top -->

  <!-- JS Global Compulsory -->
  <script src="{{asset('assets/plugins/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jquery-migrate/dist/jquery-migrate.min.js')}}"></script>
  <script src="{{asset('assets/plugins/popper.js/dist/umd/popper.min.js')}}"></script>
  <script src="{{asset('assets/plugins/bootstrap/dist/js/bootstrap.min.js')}}"></script>
  @yield('script')
  <!-- JS Implementing Plugins -->
  <script src="{{asset('assets/plugins/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jquery-ui/jquery-ui.core.min.js')}}"></script>
  <script src="{{asset('assets/plugins/jquery-ui/ui/widgets/menu.js')}}"></script>
  <script src="{{asset('assets/plugins/jquery-ui/ui/widgets/mouse.js')}}"></script>
  <script src="{{asset('assets/plugins/jquery-ui/ui/widgets/autocomplete.js')}}"></script>
  <script src="{{asset('assets/plugins/prism/prism.js')}}"></script>

  <!-- JS -->
  <script src="{{asset('assets/js/main.js')}}"></script>
  <script src="{{asset('assets/js/autocomplete.js')}}"></script>
  <script src="{{asset('assets/js/custom-scrollbar.js')}}"></script>
</body>

<!-- Mirrored from htmlstream.com/preview/awesome-dashboard-ui-kit/documentation/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 29 May 2019 15:44:13 GMT -->
</html>
