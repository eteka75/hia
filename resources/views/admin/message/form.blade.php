<div class="form-group row  {{ $errors->has('nom') ? 'has-error' : ''}}">
    {!! Form::label('nom', trans('message.nom'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::text('nom', null, ['class' => 'form-control form-control-sm', 'required' => 'required','rows'=>3]) !!}

        {!! $errors->first('nom', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('prenom') ? 'has-error' : ''}}">
    {!! Form::label('prenom', trans('message.prenom'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::text('prenom', null, ['class' => 'form-control form-control-sm', 'required' => 'required','rows'=>3]) !!}

        {!! $errors->first('prenom', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('tel') ? 'has-error' : ''}}">
    {!! Form::label('tel', trans('message.tel'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::text('tel', null, ['class' => 'form-control form-control-sm','rows'=>3]) !!}

        {!! $errors->first('tel', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', trans('message.email'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::text('email', null, ['class' => 'form-control form-control-sm','rows'=>3]) !!}

        {!! $errors->first('email', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('objet') ? 'has-error' : ''}}">
    {!! Form::label('objet', trans('message.objet'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::text('objet', null, ['class' => 'form-control form-control-sm','rows'=>3]) !!}

        {!! $errors->first('objet', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('message') ? 'has-error' : ''}}">
    {!! Form::label('message', trans('message.message'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-5">
        {!! Form::textarea('message', null, ['class' => 'form-control form-control-sm','rows'=>3]) !!}

        {!! $errors->first('message', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group row">
	<div class="col-md-3">
	</div>
    <div class="col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Sauvegarder', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
