@extends('layouts.admin')
@section('title',"Affichage du  Passation des marchés")
@section('content')
    <div >
       <div class="row">
        <div class="col-md-8 col-xs-9">
        <h1 class="h2 mb-1">Passation des marchés</h1>
        <nav aria-label="breadcrumb ">
                    <ol class="breadcrumb mb-2">
                        <li class="breadcrumb-item"><a href="{{url('/admin')}}">Tableau de bord</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> <a href="{{ url('/admin/passation-marche/') }}"> Passation des marchés</a></li>
                    </ol>
                </nav>
        </div>
        <div class="col-md-4 col-xs-3  text-right">
             {!! Form::open(['method' => 'GET', 'url' => '/admin/passation-marche', 'class' => 'navbar-form navbar-right mt30', 'role' => 'search'])  !!}
                            
            <div class="input-group h-100">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <span class="ti-search"></span>
                    </span>
                </div>
                <input class="form-control form-control-xs bg-white" type="search" placeholder="Rechercher dans les Passation des marchés" value="{{ isset($_GET['search'])?$_GET['search']:NULL }}" name="search">
                
            </div>
            {!! Form::close() !!}
        </div>
        
    </div>
        <div class="row">
            <div class="col-md-12 mb-5">
                <div class="h-100">
                    <div class="card h-100">
                        <header class="card-header">
                     <div class="row">
                                <div class="col-md-8">
                                    <h2 class="h3 card-header-title">Passation des marchés  </h2> 
                                </div>
                                <div class="col-md-4 text-right">

                                    <a href="{{ url('/admin/passation-marche') }}" title="Back"><button class="btn btn-outline-primary btn-xs mt20"><i class="ti-arrow-left" aria-hidden="true"></i> Retour</button></a>
                                    <a href="{{ url('/admin/passation-marche/' . $passationmarche->id . '/edit') }}" title="Edit PassationMarche"><button class="btn btn-primary btn-xs mt20"><i class="ti-pencil-alt" aria-hidden="true"></i>  Editer</button></a>
                                   
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['/admin/passation-marche', $passationmarche->id],
                                        'style' => 'display:inline'
                                    ]) !!}
                                        {!! Form::button('<i class="ti-trash" aria-hidden="true"></i> Supprimer', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs mt20',
                                                'title' => 'Suppression PassationMarche',
                                                'onclick'=>'return confirm("Voulez vous vraiment supprimer cet élément ?")'
                                        ))!!}
                                    {!! Form::close() !!}
                                </div>
                            </div>                           
                        </header>
                        <div class="card-body pt-3">

                                    <br/>
                                    <br/>

                                    <div class="table-responsive">
                                        <table class="table table-sm table-hover table-borderless">
                                            <tbody>
                                                <!--tr>
                                                    <th width="25%" class="">ID</th>
                                                    <td>{{ $passationmarche->id }}</td>
                                                </tr-->
                                                <tr>
                                                    <th> {{ trans('passationmarche.titre') }} </th>
                                                    <td> {{ $passationmarche->titre }} </td>
                                                </tr>
                                                <tr>
                                                    <th> {{ trans('passationmarche.fichier') }} </th>
                                                    <td> <a href="{{asset($passationmarche->fichier)}}" target="_blanck">{{asset($passationmarche->fichier)}}</a></td>
                                                </tr>
                                                <tr>
                                                    <th> {{ trans('passationmarche.type') }} </th>
                                                    <td> {{ $passationmarche->type }} </td>
                                                </tr>
                                                <tr>
                                                    <th> {{ trans('passationmarche.contenu') }} </th>
                                                    <td> {!! $passationmarche->contenu !!} </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                     </div>
            </div>
    </div>
@endsection
