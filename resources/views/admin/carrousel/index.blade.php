@extends('layouts.admin')

@section('title',"Carrousel")
@section('content')
<div >
    <div class="row">
        <div class="col-md-8 col-xs-9">
        <h1 class="h2 mb-1">Carrousels</h1>
        <nav aria-label="breadcrumb ">
                    <ol class="breadcrumb mb-2">
                        <li class="breadcrumb-item"><a href="{{url('/admin')}}">Tableau de bord</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> <a href="{{ url('/admin/carrousel/') }}"> Carrousels</a></li>
                    </ol>
                </nav>
        </div>
        <div class="col-md-4 col-xs-3  text-right">
             {!! Form::open(['method' => 'GET', 'url' => '/admin/carrousel', 'class' => 'navbar-form navbar-right mt30', 'role' => 'search'])  !!}
                            
            <div class="input-group h-100">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <span class="ti-search"></span>
                    </span>
                </div>
                <input class="form-control form-control-xs bg-white" type="search" placeholder="Rechercher dans les Carrousels" value="{{ isset($_GET['search'])?$_GET['search']:NULL }}" name="search">
                
            </div>
            {!! Form::close() !!}
        </div>
        
    </div>
 <div class="row">
            <div class="col-md-12 mb-5">
                <div class="h-100">
                    <div class="card h-100 ">
                        <header class="card-header">
                           
                            <div class="row">
                                <div class="col-md-8">
                                 <h2 class="h3 card-header-title "> Les Carrousels</h2>
                               
                                </div>
                                <div class="col-md-4 text-right">
                                <a href="{{ url('/admin/carrousel/create') }}" class="btn btn-primary btn-sm btn-xs mt-0 pull-rights " id="addbtns" title="Ajouter un Carrousel">
            <span class="ti-plus"></span> Ajouter
            </a>
                                
                                </div>
                        </header>
                        <div class="card-body pt-0">
                            <div class="table-responsive mt-3">
                            @if(isset($carrousel) && $carrousel->count())
                                <table class="table table-sm table-hover table-striped ">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{ trans('carrousel.image') }}</th>
                                            <th>{{ trans('carrousel.titre') }}</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $i=0;
                                    ?>
                                    @foreach($carrousel as $item)
                                        <?php
                                            $i++;
                                        ?>
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td> 
                                                @if($item->image)

                                                <img src="{{asset($item->image)}}" width="200px" alt="image">
                                                @endif
                                            </td>
                                            <td>

                                                <b> {{ $item->titre }}</b><br>  
                                                <div class="text-muted"> <i> {{ substr($item->sous_titre,0,80) }}</i> </div>
                                                 @if($item->etat==1 )
                                              <span class="badge badge-success"> Activé </span>
                                              @else
                                              <span class="badge badge-danger"> Désactivé </span>
                                              @endif

                                            </td>
                                            
                                            <td width="10%" nowrap="nowrap">
                                                <a href="{{ url('/admin/carrousel/' . $item->id) }}" title="Voir ce Carrousel"><i class="ti-eye" ></i> </a>&nbsp;&nbsp;&nbsp;
                                                <a href="{{ url('/admin/carrousel/' . $item->id . '/edit') }}" title="Modifier ce Carrousel"><i class="ti-pencil-alt"></i></a> 
                                                {!! Form::open([
                                                    'method'=>'DELETE',
                                                    'url' => ['/admin/carrousel', $item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                    {!! Form::button('<i class="ti-trash" aria-hidden="true"></i>', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-sm text-danger btn-circle  btn-xs',
                                                            'title' => 'Suppression de ce Carrousel',
                                                            'onclick'=>'return confirm("Voulez vous vraiment supprimer cet élément ?")'
                                                    )) !!}
                                                {!! Form::close() !!}

                                            </td>

                                        </tr>
                                    @endforeach

                                 </tbody>
                                </table>
                                 <div class="pagination-wrapper"> {!! $carrousel->appends(['search' => Request::get('search')])->render() !!} </div> 
                                @else($i==0)
                                   
                                    <div class="col-md-12 ">
                                     <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <div class="card-body borderc text-center text-muted">
                                            <h1 class="h1"><i class="ti-harddrives"></i></h1>
                                               <h4 class="text-muted">   Aucun carrousel disponible <br>   
                                               <small>  Les Carrousels enrégistrés apparaissent ici</small></h4>  
                                               <a href="{{ url('/admin/carrousel/create') }}" class="btn btn-outline-primary  btn-xs  btn-minimt-0 pull-rights " id="addbtns" title="Ajouter un Dossier de credit">
                                               <span class="ti-plus"></span> Ajouter un carrousel 
                                               </a>
                                            </div>
                                          </div>
                                    </div>
                                     </div>
                                @endif
                               
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
