@section('css')
<link href="{{asset('assets/plugins/summernote/dist/summernote.css')}}" rel="stylesheet">
<style type="text/css">
.note-popover .popover-content, .panel-heading.note-toolbar{
    background: #eaeff5;
} 
</style>
@endsection
@section('script')
<script src="{{asset('assets/plugins/summernote/dist/summernote.js')}}"></script>

<script type="text/javascript">
var $editor = $('.summernote');

$editor.summernote({
    disableDragAndDrop: true,
    callbacks: {
        // Clear all formatting of the pasted text
        onPaste: function (e) {
            var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
            e.preventDefault();
            setTimeout(function () {
                document.execCommand('insertText', false, bufferText);
            }, 10);
        }
    },
    toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['fontsize', ['fontsize']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'hr']],
        ['view', ['fullscreen']],
                //=>, 'superscript', 'subscript'
//    ['help', ['help']]
    ],
//    height: 250,
    height: 200,
    placeholder: 'Rédigez votre actualité',
    dialogsInBody: true,
    lang: 'fr-FR',
});


</script>

@endsection
<div class="form-group row  {{ $errors->has('nom') ? 'has-error' : ''}}">
    {!! Form::label('nom', trans('membre.nom'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::text('nom', null, ['class' => 'form-control form-control-sm', 'required' => 'required','rows'=>3]) !!}

        {!! $errors->first('nom', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('prenom') ? 'has-error' : ''}}">
    {!! Form::label('prenom', trans('membre.prenom'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::text('prenom', null, ['class' => 'form-control form-control-sm', 'required' => 'required','rows'=>3]) !!}

        {!! $errors->first('prenom', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('poste') ? 'has-error' : ''}}">
    {!! Form::label('poste', trans('membre.poste'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::text('poste', null, ['class' => 'form-control form-control-sm','rows'=>3]) !!}

        {!! $errors->first('poste', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('photo') ? 'has-error' : ''}}">
    {!! Form::label('photo', trans('membre.photo'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::file('photo',  ['class' => 'form-control form-control-sm', 'required' => 'required','rows'=>3]) !!}

        {!! $errors->first('photo', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row  {{ $errors->has('biographie') ? 'has-error' : ''}}">
    {!! Form::label('biographie', trans('membre.biographie'), ['class' => 'col-md-3 text-right control-label']) !!} :
    <div class="col-md-8">
        {!! Form::textarea('biographie', null, ['class' => 'form-control form-control-sm summernote','rows'=>3]) !!}

        {!! $errors->first('biographie', '<p class="form-text text-danger help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group row">
	<div class="col-md-3">
	</div>
    <div class="col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Sauvegarder', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
