<div>
	<div class="fb-page" data-href="https://web.facebook.com/hia.chu.parakou" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>

	<div id="rs" class="tab-panetab-panefadeinactive">
		
                <!--div class="fb-page" data-href="https://www.facebook.com/UPBENIN/" data-tabs="timeline" data-width="290" data-height="400" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/UPBENIN/"><a href="https://www.facebook.com/UPBENIN/">Université de Parakou</a></blockquote></div></div-->

                <div class="fb-page" data-href="https://web.facebook.com/hiachupkou" data-tabs="timeline" data-width="350" data-height="450" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
            </div>
	@if(isset($top_actus))
<div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; top: 25px; left: 957.925px;">
	<!-- Sidebar Small List Start -->
	<div class="widget fbt-vc-inner clearfix">
		<div class="title-wrapper color-3">
			<h2><span>A lire dans les actualités</span></h2>
		</div>
		@foreach($top_actus as $actu)
		<div class="post-item small">
			<div class="row">
				<div class="col-sm-4 col-xs-3">
					<div class="img-thumb">
						
						<a href="{{url('/')}}"><div class="fbt-resize" style="background-image: url('{{asset($actu->photo)}}')"></div></a>
					</div>
				</div>
				<div class="col-sm-8 col-xs-9 no-padding-left">
					<div class="post-content">
						<a href="{{route('web.actualite',$actu->slug)}}"><h3>{{str_limit($actu->titre,65)}}</h3></a>
						<div class="post-info clearfix">
							<span>{{date('M d, Y',strtotime($actu->created_at))}}</span>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		@endforeach
			<a class="btn btn-default btn-block" href="{{route('web.actualites')}}"><i class="fa fa-plus">	</i> Toutes les actualités</a>
	</div>
	 @if(isset($breves))
<div class="widget sidebar-carousel clearfix">
    <div class="title-wrapper color-8">
        <h2><span>A lire dans les brèves</span></h2>
    </div>

    <div class="carousel-content-box owl-wrapper clearfix">
        <div class="owl-carousel" data-num="1">
            @foreach($breves as $breve)
            <div class="item fbt-hr-crs">
                <div class="post-item clearfix">
                    <div class="img-thumb">
                        <a href="{{route('web.breve',$breve->slug)}}"><div class="fbt-resize" style="background-image: url('{{asset($breve->photo)}}')"></div></a>
                        <div class="img-credits">
                            <a href="{{route('web.breve',$breve->slug)}}"><h3>{{$breve->titre}}.</h3></a>
                            <div class="post-info clearfix">
                                <span>{{date('M d, Y',strtotime($breve->created_at))}}</span>
                            </div>
                            <div class="text-content">
                                <?php
                                    $content= strip_tags($breve->contenu);
                                ?>
                                <p>
                                    {{str_limit($content,200)}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div>	
		<a class="btn btn-secondary color-8 text-white rond0  btn-block" href="{{route('web.breves')}}"> <i class="fa fa-plus ">	</i> Plus de Brèves</a>
        </div>
    </div>
</div>
@endif

	<div class="widget tag-widget">
		<div class="title-wrapper color-2">
			<h2><span>Classement par catégorie</span></h2>
		</div>
		@if(isset($categories))
		<ul class="tag-list">
		@foreach($categories as $cat)
		
			<li><a href="{{route('web.actualite.categorie',$cat->slug)}}">{{$cat->nom}}</a></li>
		
		@endforeach
		</ul>
		@endif
	</div>
	
	
	
</div>
@endif

</div>