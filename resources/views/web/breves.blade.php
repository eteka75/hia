@extends('layouts.web')
@section('title',"Toutes les brèves")
@section('content')
<div class="container">	
<div class="row">	
	<div class="outer-wrapper clearfix">	
	<div class="fbt-col-lg-9 col-md-8 col-sm-6 post-wrapper single-post" style="transform: none;">
						<div class=""><h2 class="page-header">Brèves</h2></div>
								<div class="row">
									@if(isset($datas))
									@foreach($datas as $a)

									<div class="col-md-4 col-sm-12 col-xs-6 fbt-vc-inner post-grid clearfix">
										<div class="post-item clearfix">
											<div class="img-thumb">
												<a href="{{route('web.actualite',$a->slug)}}"><div class="fbt-resize" style="background-image: url('{{asset($a->photo)}}')"></div></a>
											</div>
											<div class="post-content">
												
												<a href="{{route('web.breve',$a->slug)}}"><h5>{{str_limit($a->titre,50)}}</h5></a>
												<div class="post-info clearfix">
													
													<span class="fa fa-clock-o"></span>
													<span>{{date('M d, Y',strtotime($a->created_at))}}</span>
												</div>
											</div>
										</div>
									</div>
									@endforeach
									@endif
								</div>
								<!-- Pagination Start -->
								<div class="pagination-box clearfix">
									{{$datas->links()}}
								</div><!-- Pagination End -->
							
	</div>
	<div class="fbt-col-lg-3 col-md-4 col-sm-6 post-sidebar clearfix" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
		@include("web.includes.right1")
	</div>
	</div>
</div>
</div>
@endsection('content')
