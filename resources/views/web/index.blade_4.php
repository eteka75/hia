
<!DOCTYPE html>
<html dir="ltr" lang="en-US" class="no-js"> 

<!-- Mirrored from fbtemplates.net/html/glossymag/classic.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 15 Mar 2019 08:15:59 GMT -->
<head>
  <!-- Basic -->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta name="keywords" content="Responsive, HTML5, Template" />
  <meta name="description" content="Responsive HTML5 Template" />
  
  <title>Glossy Magazine</title>
  
  <link rel="shortcut icon" href="favicon.ico">
  
  <!-- Font Awesome -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/web/font-awesome/css/font-awesome.min.css')}}" media="screen">
  
  <!-- Stylesheets -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets/web/css/bootstrap.min.css')}}" media="screen">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/web/css/colors.css')}}" media="screen">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/web/css/owl.carousel.css')}}" media="screen">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/web/css/owl.theme.css')}}" media="screen">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/web/css/style.css')}}" media="screen">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/web/css/classic.css')}}" media="screen">
  
  <!--[if lt IE 9]>
    <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script type="text/javascript" src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
      .headline-title {
    background: #ec0f0f;
    }
    .header-wrapper{
        padding: 0;
    }
    .logo img{
        margin-top: 40px;
        margin-left: 10px;
    }
    .pad0{padding: 0}
    .headline-wrapper{
        background: #065d68;
        background: #009966;/*#166374;*/
        border-bottom: 2px solid #ffe422;
    }
    ul.ticker li a{color: #ffe422}
    .navbar-default.dark {
    background: #2a5d68;
    background: #232f3e;
    
    border-bottom: 3px solid #f4ee2d;
    }
    ul.ticker li a:hover {
    //color: #ec0f0f;
    transition: ease .28s;
    //letter-spacing: 2px
    }
    ul.dropdown-menu > li {
    
    background: #064460;
    border-top:1px solid #1c5c79;
    }
    a:link {
    -webkit-tap-highlight-color: #ffcb0c;
    }
    ul.dropdown-menu.fullwidth>li{/*background: #1d2863;*/background: #064460;}
   .navbar-default .navbar-toggle{border-color: #ffffff;border-radius: 0px}
   .navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle:hover{background: rgba(255,255,255,.45);}
   .navbar-default .navbar-toggle .icon-bar{background: #ffffff;}
.ad-space{
    max-height: 150px;
}
    .navbar-default{
        height: 52px;
        background: #1d2863;
        background: #064460;
        border-bottom: 3px solid #03A9F4;}
    ul.dropdown-menu > li > a:focus, ul.dropdown-menu > li > a:hover{background: #03A9F4}
    .navbar-default .navbar-nav > li > a{color: #ffffff}
    @media (max-width: 767px){
        .navbar-default .navbar-nav > li > a{color: #333333;text-transform: uppercase;}
        .navbar-default .navbar-nav .open .dropdown-menu>li>a{color: #ffffff}
        .dropdown .caret{float: right;margin-right: 15px;margin-top: 15px}
    }
  </style>

</head>
<body>
    <div class="navbar-fixed-top"></div>
    <div class="container-box">
        <!-- Headline Start -->
        <section id="newsticker">
            <div class="headline-wrapper">
                <div class="container">
                    <div class="row">
                        <!-- Newsticker start -->
                        <div class="col-md-2 col-sm-3 col-xs-5">
                            <div class="headline-title">                
                                <h5>COMMUNIQUE</h5>
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-9 col-xs-7 no-padding">
                            <ul class="ticker clearfix">
                                <li><a href="classic-post.html">Ei his graeci option officiis, no oratio vocent efficiendi vix.</a></li>
                                <li><a href="classic-post.html">Nam iusto delicata ne, eam dolore singulis maiestatis ex.</a></li>
                                <li><a href="classic-post.html">Women in Hollywood, according to new study</a></li>
                                <li><a href="classic-post.html">The age of first-time mothers is rising faster in the US</a></li>
                                <li><a href="classic-post.html">Baby brain doesn't exist say scientists</a></li>
                                <li><a href="classic-post.html">How can Build a Better Connection Between the Mind and Body</a></li>
                                <li><a href="classic-post.html">How Triathletes Can Maximize Their Swim Block Training</a></li>
                            </ul>
                        </div><!-- Newsticker end -->
                        <!-- Social Icons Start -->
                        <div class="col-md-3 hidden-sm hidden-xs">
                            <div class="fa-icon-wrap">
                                <a class="facebook" href="#" data-toggle="tooltip" data-placement="left" title="Facebook"><i aria-hidden="true" class="fa fa-facebook"></i></a>
                                <a class="twitter" href="#" data-toggle="tooltip" data-placement="left" title="Twitter"><i aria-hidden="true" class="fa fa-twitter"></i></a>
                                <a class="youtube" href="#" data-toggle="tooltip" data-placement="left" title="Youtube"><i aria-hidden="true" class="fa fa-youtube"></i></a>
                            </div>
                        </div><!-- Social Icons End -->
                    </div>
                </div>
            </div>
        </section><!-- Headline End -->
        
        <!-- Header Start -->
        <section class="header-wrapper clearfix">
            <div class="container pad0">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <h1 class="logo"><a href="{{url('/')}}"><img class="img-responsive" src="assets/images/logo-hia.png" alt="logo"/></a></h1>
                    </div>
                    <div class="col-md-1 hidden-sm"></div>
                    <div class="col-md-8 col-sm-9 pad0">
                        <div class="ad-space ads-768">
                            <img src="{{asset('assets/web/img/baniere-min.jpg')}}" alt="header-ad"/>

                        </div>
                        <!--div class="ad-space ads-468">
                            <a href="#" target="_blank"><img src="assets/web/img/468x60.jpg" alt="header-ad"/></a>
                        </div-->
                    </div>
                </div>
            </div>
        </section><!-- Header End -->
        
        <!-- Menu Navigation Start -->
        <div class="navbar darks navbar-default megamenu clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="navbar-header">
                            <button type="button" data-toggle="collapse" data-target="#mainmenu" class="navbar-toggle">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.html"><img class="img-responsive" src="assets/images/logo-hia.png" alt="HIA-CHU"/></a>
                        </div>
                        <div id="mainmenu" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li class="{{isset($page) && $page=='home'?'active':''}}"><a  href="{{url('/')}}"><i class="fa fa-home"></i></a></li>
                                <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">L'Hôpital <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                         <li>
                                            <a href="{{route('web.presentation')}}"><i class="ti-plus"> </i>Présentation</a>
                                        </li>
                                        <li>
                                            <a href="{{route('web.direction_generale')}}">La direction Générale</a>
                                        </li>
                                        <li>
                                            <a href="{{route('web.projet_rapport')}}">Etudes projets et Rapports</a>
                                        </li>
                                        <li>
                                            <a href="{{route('web.partenariat_jumelage')}}">Partenariats et Jumélage</a>
                                        </li>
                                        <li>
                                            <a href="{{route('web.oraganisation_travaux')}}">Organisation des travaux</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown {{isset($page) && $page=='services'?'active':''}}"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Services <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                         <li>
                                            <a href="{{route('web.services_medico_tech')}}"><i class="ti-plus"> </i>Services Médico-Techniques </a>
                                        </li>
                                       <li>
                                            <a href="{{route('web.services_admin_fin')}}"><i class="ti-plus"> </i>Services Administratifs et Finances</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown full-cont {{isset($page) && $page=='actualite'?'active':''}}"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Actualités <b class="caret"></b></a>
                                    <!-- Mega Menu Start -->
                                    <ul class="dropdown-menu fullwidth">
                                        <li class="default clearfix">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-6 mega-item">
                                                    <div class="img-thumb">
                                                        <a href="#"><div class="fbt-resize" style="background-image: url('assets/web/img/img-1.jpg')"></div></a>
                                                        <div class="img-credits">
                                                            <h3>Fringilla pellentesque leo sed dolor quam velit.</h3>
                                                            <div class="post-info">
                                                                <span>Sep 12, 2016</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 mega-item">
                                                    <div class="img-thumb">
                                                        <a href="#"><div class="fbt-resize" style="background-image: url('img/img-2.jpg')"></div></a>
                                                        <div class="img-credits">
                                                            <h3>Orci in aliquam diam, felis pede, wisi diam mollis, sit lobortis eget.</h3>
                                                            <div class="post-info">
                                                                <span>Sep 17, 2016</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 mega-item">
                                                    <div class="img-thumb">
                                                        <a href="#"><div class="fbt-resize" style="background-image: url('img/img-3.jpg')"></div></a>
                                                        <div class="img-credits">
                                                            <h3>Pellentesque feugiat neque a placerat nec et.</h3>
                                                            <div class="post-info">
                                                                <span>Sep 19, 2016</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 mega-item">
                                                    <div class="img-thumb">
                                                        <a href="#"><div class="fbt-resize" style="background-image: url('img/img-4.jpg')"></div></a>
                                                        <div class="img-credits">
                                                            <h3>Curabitur vel, magnis duis faucibus nam magna donec justo, tortor quam.</h3>
                                                            <div class="post-info">
                                                                <span>Sep 22, 2016</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul><!-- Mega Menu End -->
                                </li>
                                <li class="dropdown {{isset($page) && $page=='infos'?'active':''}}"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Infos <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                         <li>
                                            <a href="{{route('web.services_medico_tech')}}"><i class="ti-plus"> </i>Informations utiles </a>
                                        </li>
                                       <li>
                                            <a href="{{route('web.services_admin_fin')}}"><i class="ti-plus"> </i>Autres Informations</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown {{isset($page) && $page=='prmp'?'active':''}}"><a href="#" data-toggle="dropdown" class="dropdown-toggle">PRMP <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                         <li>
                                            <a href="{{route('web.demande_cotisation')}}">Demandes de cotation</a>
                                        </li>
                                        <li>
                                            <a href="{{route('web.avis_attribution')}}">Avis d'attributions</a>
                                        </li>
                                    </ul>
                                </li>
                                <!--li><a href="#">Sports</a></li-->
                                <li class="{{isset($page) && $page=='contact'?'active':''}}"><a href="#">Contact</a></li>
                                
                            </ul>
                            <!-- Search Form start -->
                            <form class="navbar-form dark_ navbar-right" role="search">
                                <input type="text" id="search" name="search" placeholder="Rechercher...">
                                <button type="submit" id="search-submit"><i class="fa fa-search"></i></button>
                            </form><!-- Search Form end -->
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- Menu Navigation End -->
        
        <!-- Featured Slide Start -->
        <main id="main" class="content">  
        @yield('content')
        </main>
        <section class="footer-wrapper clearfix">
            <div class="container">
                <div class="row">
                    <!-- Footer Widgets Start -->
                    <div class="col-md-3 col-sm-6">
                        <!-- Text Widget Start -->
                        <div class="widget text-widget">
                            <div class="widget-title">
                                <h2>About Us</h2>
                            </div>
                            <img class="img-responsive" src="assets/web/img/logo-2.png" alt="logo"/>
                            <p><span class="fbt-tooltip" data-toggle="tooltip" title="Favorite Blogger Templates">fbtemplates</span> in Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Nunc interdum metus vitae dui tempus ornare. Nulla facilisi. 
                            Vivamus id eros sit amet lorem condimentum ultrices. 
                            Phasellus imperdiet, magna at sodales dictum, nibh mauris tempor nulla, in consequat est purus eget.</p>
                            <!-- Social Icons Start -->
                            <div class="fa-icon-wrap">
                                <a class="facebook" href="#"><i aria-hidden="true" class="fa fa-facebook"></i></a>
                                <a class="twitter" href="#"><i aria-hidden="true" class="fa fa-twitter"></i></a>
                                <a class="instagram" href="#"><i aria-hidden="true" class="fa fa-instagram"></i></a>
                                <a class="google-plus" href="#"><i aria-hidden="true" class="fa fa-google-plus"></i></a>
                                <a class="dribbble" href="#"><i aria-hidden="true" class="fa fa-dribbble"></i></a>
                                <a class="youtube" href="#"><i aria-hidden="true" class="fa fa-youtube"></i></a>
                            </div><!-- Social Icons End -->
                        </div><!-- Text Widget End -->
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <!-- Categories Widget Start -->
                        <div class="widget categories-widget">
                            <div class="widget-title">
                                <h2>Hot Categories</h2>
                            </div>
                            <ul class="category-list">
                                <li><a href="#">Sport <span>32</span></a></li>
                                <li><a href="#">Fashion <span>39</span></a></li>
                                <li><a href="#">Technology <span>12</span></a></li>
                                <li><a href="#">Music <span>15</span></a></li>
                                <li><a href="#">Business <span>12</span></a></li>
                                <li><a href="#">Politics <span>17</span></a></li>
                                <li><a href="#">Lifestyle <span>43</span></a></li>
                            </ul>
                        </div><!-- Categories Widget End -->
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <!-- Footer Small List Start -->
                        <div class="widget fbt-vc-inner clearfix">
                            <div class="widget-title">
                                <h2>Most Commented</h2>
                            </div>
                            <div class="post-item small">
                                <div class="row">
                                    <div class="col-sm-4 col-xs-3">
                                        <div class="img-thumb">
                                            <a href="classic-post.html"><div class="fbt-resize" style="background-image: url(assets/web/img/img-4.jpg)"></div></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-8 col-xs-9 no-padding-left">
                                        <div class="post-content">
                                            <a href="classic-post.html">
                                                <h3>Aliquam metus mauris, litora orci ligula.</h3>
                                            </a>
                                            <div class="post-info clearfix">
                                                <span>Mar 3, 2016</span>
                                                <span>-</span>
                                                <span><i class="fa fa-commenting-o" aria-hidden="true"></i> 32 Comments</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-item small">
                                <div class="row">
                                    <div class="col-sm-4 col-xs-3">
                                        <div class="img-thumb">
                                            <a href="classic-post.html">
                                                <div class="fbt-resize" style="background-image: url('assets/web/img/img-31.jpg')"></div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-8 col-xs-9 no-padding-left">
                                        <div class="post-content">
                                            <a href="classic-post.html">
                                                <h3>Dolor ut a est maecenas, neque odio dui leo lacus varius.</h3>
                                            </a>
                                            <div class="post-info clearfix">
                                                <span>Mar 2, 2016</span>
                                                <span>-</span>
                                                <span><i class="fa fa-commenting-o" aria-hidden="true"></i> 29 Comments</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-item small">
                                <div class="row">
                                    <div class="col-sm-4 col-xs-3">
                                        <div class="img-thumb">
                                            <a href="classic-post.html">
                                                <div class="fbt-resize" style="background-image: url(assets/web/img/img-3.jpg)"></div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-8 col-xs-9 no-padding-left">
                                        <div class="post-content">
                                            <a href="classic-post.html">
                                                <h3>Etiam duis nunc dui ad sagittis, mauris at rem, in nunc.</h3>
                                            </a>
                                            <div class="post-info clearfix">
                                                <span>Feb 23, 2016</span>
                                                <span>-</span>
                                                <span><i class="fa fa-commenting-o" aria-hidden="true"></i> 26 Comments</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Footer Small List End -->
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <!-- Flickr Widget Start -->
                        <div class="widget flickr-widget clearfix">
                            <div class="widget-title">
                                <h2>Flickr Photos</h2>
                            </div>
                            <div class="flickr-gallery clearfix">
                                <div class="row">
                                    <div class="gallery-img">
                                        <div class="col-xs-4 padding-1">
                                            <div class="post-item clearfix">
                                                <div class="img-thumb">
                                                    <a href="classic-post.html"><div class="fbt-resize" style="background-image: url('assets/web/img/img-10.jpg')"></div></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 padding-1">
                                            <div class="post-item clearfix">
                                                <div class="img-thumb">
                                                    <a href="classic-post.html"><div class="fbt-resize" style="background-image: url('assets/web/img/img-2.jpg')"></div></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 padding-1">
                                            <div class="post-item clearfix">
                                                <div class="img-thumb">
                                                    <a href="classic-post.html"><div class="fbt-resize" style="background-image: url('assets/web/img/img-5.jpg')"></div></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 padding-1">
                                            <div class="post-item clearfix">
                                                <div class="img-thumb">
                                                    <a href="classic-post.html"><div class="fbt-resize" style="background-image: url('assets/web/img/img-8.jpg')"></div></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 padding-1">
                                            <div class="post-item clearfix">
                                                <div class="img-thumb">
                                                    <a href="classic-post.html"><div class="fbt-resize" style="background-image: url('assets/web/img/img-3.jpg')"></div></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 padding-1">
                                            <div class="post-item clearfix">
                                                <div class="img-thumb">
                                                    <a href="classic-post.html"><div class="fbt-resize" style="background-image: url('assets/web/img/img-28.jpg')"></div></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="fl-more" href="#">View more photos...</a>
                        </div><!-- Flickr Widget End -->
                    </div>
                </div><!-- Footer Widgets End -->
                <!-- Copyrights Start -->
                <div class="copyrights">
                    <div class="row">
                        <div class="col-md-6">
                            <p>COPYRIGHT &copy; 2017 glossymagazine.com</p>
                        </div>
                        <div class="col-md-6">
                            <div class="fbt-footer-nav">
                                <ul>
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="#">FAQ</a></li>
                                    <li><a href="#">About</a></li>
                                    <li><a href="classic-contact.html">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- Copyrights End -->
            </div>
        </section><!-- Footer Sidebar End -->
        
        <a href="#" id="BackToTop"><i class="fa fa-angle-up"></i></a>
    </div>
    <div class="navbar-fixed-bottom"></div>
    
    <!-- JAVASCRIPTS -->
    <script type="text/javascript" src="{{asset('assets/web/js/plugins/jquery-1.12.3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/web/js/plugins/smoothscroll.js')}}"></script>
    @yield('script')
    <script type="text/javascript" src="{{asset('assets/web/js/plugins/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/web/js/plugins/cycle.all.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/web/js/plugins/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/web/js/plugins/resizesensor.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/web/js/plugins/theia-sticky-sidebar.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/web/js/functions/main.js')}}"></script>
</body>

<!-- Mirrored from fbtemplates.net/html/glossymag/classic.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 15 Mar 2019 08:16:17 GMT -->
</html>