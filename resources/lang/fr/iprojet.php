<?php

return [
    'nom_projet' => 'Nom du projet',
'rfc_rcc' => 'RFC/RCC',
'forme_juridique' => 'Forme juridique',
'secteur_activite' => "Secteur d'activité",
'activite' => 'Activité',
'nb_employe' => "Nombre d'Employé",
'cout_projet' => 'Coût du projet (en FCFA)',
'apport_personnel' => 'Apport personnel',
'finacement_sollicite' => 'Financement sollicité',
'resume_projet' => 'Résumé du projet',
'fichier' => "Fichier d'identification projet",
];
