<?php

return [
    'nom' => 'Nom',
'prenom' => 'Prénom',
'image' => 'Image',
'type' => 'Type de compte',
'photo' => 'Photo de profil',
'email' => 'Email',
'pseudo' => 'Pseudo',
'etat' => 'Etat',
];
