<?php

return [
    'nom' => 'Nom du projet',
'formejuridique' => 'Forme juridique',
'localisation' => 'Lieu',
'refrcc' => 'Références',
'coutduprojet' => 'Coût du projet(CFA)',
'apportpersonnel' => 'Apport personnel(CFA)',
'resume' => 'Résume du projet',
'financementsolllicite' => 'Financement solllicité(CFA)',
'nbreemploye' => "Nombre d'employés",
'degrederenouvelement' => 'Degré de renouvelement',
'secteuractivite' => "Secteur d'activité",
'id_entreprise' => 'Entreprise',
];
