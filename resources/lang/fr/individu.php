<?php

return [
    'type' => 'Type',
'nom_ind' => 'Nom de famille ',
'prenom_ind' => 'Prénom(s)',
'surnom_ind' => 'Surnom ',
'date_adhesion' => "Date d'adhésion",
'sexe' => 'Sexe',
'piece' => "Pièce d'indentité",
'nature_piece' => 'Nature Pièce',
'date_naissance' => 'Date de naissance',
'lieu_naissance' => 'Lieu naissance',
'niveau_etude' => "Niveau d'étude",
'profession' => 'Profession',
'quartier' => 'Quartier',
'maison' => 'Maison',
'lot' => 'Lot',
'nb_annee_a_cette_adresse' => "Nombre d'annee à cette adresse",
'tel' => 'Téléphone',
'email' => 'Email',
'sit_mat' => 'Situation Matrimoniale',
'nbenfant_charge' => 'Nombre enfant charge',
'autre_nb_enfant' => 'Autre enfant enfant à charge',
'nom_pere' => 'Nom du père',
'nom_mere' => 'Nom du père',
'fichier' => 'Fichier',
];
