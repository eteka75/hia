<?php

return [
    'jour' => 'Jour',
'date' => "Date de l'exercice",
'mois' => 'Mois',
'annee' => 'Annee',
'date_exercice' => 'Date exercice',
'superficecultivee' => 'Superficie cultivée',
'superficieloue' => 'Superficie louée',
'superficedisponocultive' => 'Superficie cultivable',
'id_activite' => 'Activité',
'id_saison' => 'Saison',
'id_entreprise' => 'Entreprise',
'id_site' => 'Site',
];
