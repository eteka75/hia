<?php

return [
    'nom' => 'Nom de famille',
'prenom' => 'Prénom',
'profession' => 'Profession',
'telephone' => 'Téléphone',
'adresse' => 'Adresse (domicile)',
'numerocompte' => 'Numéro compte',
'date_demande' => 'Date de la demande',
'montant' => 'Montant sollicité (FCFA)',
'duree' => 'Durée de crédit (en mois)',
'nbmois' => 'Durée de crédit (en mois)',
'objet_financement' => 'Objet du financement',
'garantie' => 'Garantie',
'fichier' => 'Joindre le fichier de la lettre de demande de crédit',
];
