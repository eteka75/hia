<?php
#ETEKA Modification
namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Publication;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;

class PublicationController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $perPage = 10;
        $uid=Auth::user()?Auth::user()->id:0;

        if (!empty($keyword)) {
            $publication = Publication::where('titre', 'LIKE', "%$keyword%")
				->orWhere('type', 'LIKE', "%$keyword%")
				->orWhere('contenu', 'LIKE', "%$keyword%")
				->orWhere('image', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $publication = Publication::paginate($perPage);
        }

        return view('admin.publication.index', compact('publication'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $replace="/create";
        return view('admin.publication.create',compact('replace'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'type' => 'required'
		]);
        $requestData = $request->all();
        

        if ($request->hasFile('image')) {
            foreach($request['image'] as $file){
                $uploadPath = ('/uploads/image');

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['image'] = $fileName;
            }
        }


        $did=Auth::user()?Auth::user()->id:0;
        $requestData["id_user"]=$did;
        
        Publication::create($requestData);

        Session::flash('success', 'Publication ajoutée avec succès !');

        return redirect('admin/publication');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $publication = Publication::findOrFail($id);
        $replace="/".$id;
        return view('admin.publication.show', compact('publication','replace'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $publication = Publication::findOrFail($id);
        $replace="/".$id."/edit";
        return view('admin.publication.edit', compact('publication','replace'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'type' => 'required'
		]);
        $requestData = $request->all();
        

        if ($request->hasFile('image')) {
            foreach($request['image'] as $file){
                $uploadPath = ('/uploads/image');

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['image'] = $fileName;
            }
        }

        $publication = Publication::findOrFail($id);
        $publication->update($requestData);

        Session::flash('info', 'La Mise à jour de la "Publication" a été effectuée  !');

        return redirect('admin/publication');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Publication::destroy($id);

        Session::flash('danger', 'La suppression de la "Publication" a été effectuée !');

        return redirect('admin/publication');
    }
}
