<?php
#ETEKA Modification
namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Communique;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;

class CommuniqueController extends Controller
{
    protected $page="comm";
    public function __construct()
    {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $perPage = 10;
        $uid=Auth::user()?Auth::user()->id:0;

        if (!empty($keyword)) {
            $communique = Communique::where('titre', 'LIKE', "%$keyword%")
				->orWhere('slug', 'LIKE', "%$keyword%")
				->orWhere('contenu', 'LIKE', "%$keyword%")
				->orWhere('id_user', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $communique = Communique::paginate($perPage);
        }
        $page=$this->page;
        return view('admin.communique.index', compact('communique','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $replace="/create";
        return view('admin.communique.create',compact('replace'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'contenu' => 'required'
		]);
        $requestData = $request->all();
        

        $did=Auth::user()?Auth::user()->id:0;
        $requestData["id_user"]=$did;
        $requestData["slug"]=substr(str_slug($requestData["titre"]),0,250);
        
        Communique::create($requestData);

        Session::flash('success', 'Communique ajouté avec succès !');

        return redirect('admin/communique');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $communique = Communique::findOrFail($id);
        $replace="/".$id;
        $page=$this->page;
        return view('admin.communique.show', compact('communique','replace','edit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $communique = Communique::findOrFail($id);
        $replace="/".$id."/edit";
        $page=$this->page;
        return view('admin.communique.edit', compact('communique','replace','edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'contenu' => 'required'
		]);
        $requestData = $request->all();
        $requestData["slug"]=substr(str_slug($requestData["titre"]),0,250);
        $communique = Communique::findOrFail($id);

        //dd($requestData);
        $communique->update($requestData);


        Session::flash('info', 'La Mise à jour de "Communique" a été effectuée  !');

        return redirect('admin/communique');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Communique::destroy($id);

        Session::flash('danger', 'La suppression de "Communique" a été effectuée !');

        return redirect('admin/communique');
    }
}
