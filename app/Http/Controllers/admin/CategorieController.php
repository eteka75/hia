<?php
#ETEKA Modification
namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Categorie;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;

class CategorieController extends Controller
{
    protected $page="cat";
    public function __construct()
    {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $perPage = 10;
        $uid=Auth::user()?Auth::user()->id:0;

        if (!empty($keyword)) {
            $categorie = Categorie::where('nom', 'LIKE', "%$keyword%")
				->orWhere('slug', 'LIKE', "%$keyword%")
				->orWhere('detail', 'LIKE', "%$keyword%")
				->orWhere('id_user', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $categorie = Categorie::paginate($perPage);
        }
        $page=$this->page;
        return view('admin.categorie.index', compact('categorie','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $replace="/create";
        $page=$this->page;
        return view('admin.categorie.create',compact('replace','page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'nom' => 'required'
		]);
        $requestData = $request->all();
        

        $did=Auth::user()?Auth::user()->id:0;
        $requestData["id_user"]=$did;
        $lml=str_slug($requestData["nom"]);
        $requestData["slug"]=substr($lml,0,250); 
        
        Categorie::create($requestData);

        Session::flash('success', 'Categorie ajouté avec succès !');

        return redirect('admin/categorie');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $categorie = Categorie::findOrFail($id);
        $replace="/".$id;
        $page=$this->page;
        return view('admin.categorie.show', compact('categorie','replace','page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $categorie = Categorie::findOrFail($id);
        $replace="/".$id."/edit";
        $page=$this->page;
        return view('admin.categorie.edit', compact('categorie','replace','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'nom' => 'required'
		]);
        $requestData = $request->all();
        $lml=str_slug($requestData["nom"]);
        $requestData["slug"]=substr($lml,0,250); 
        
        $categorie = Categorie::findOrFail($id);
        $categorie->update($requestData);

        Session::flash('info', 'La Mise à jour de "Categorie" a été effectuée  !');

        return redirect('admin/categorie');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Categorie::destroy($id);

        Session::flash('danger', 'La suppression de "Categorie" a été effectuée !');

        return redirect('admin/categorie');
    }
}
