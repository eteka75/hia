<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User ;
use \App\Models\GalerieImage ;
use \App\Models\Ressource ;
use \App\Models\Message ;
use \App\Models\Actualite;
use Illuminate\Support\Facades\Hash;
use Auth;
use Session;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
     protected $page="home";
    public function __construct()
    {
       // $this->middleware('auth');
    }

   
    public function index()
    {  
        $page=$this->page;
        $nb_actu=Actualite::select('id')->where('etat',1)->count();
        $nb_g_image=GalerieImage::select('id')->where('etat',1)->count();
        $nb_message=Message::select('id')->count();
        $nb_rs=Ressource::select('id')->where('etat',1)->count();
        $taux_actu=$taux_msg=$taux_image=$taux_fichiers=0;
        $total=$nb_actu+$nb_g_image+$nb_message+$nb_rs;
        
        if($nb_actu>0){ $taux_actu=ceil(($nb_actu/$total)*100);}
        if($nb_message>0){$taux_msg=ceil(($nb_message/$total)*100);}
        if($nb_g_image>0){$taux_image=ceil(($nb_g_image/$total)*100);}
        if($nb_rs>0){$taux_fichiers=ceil(($nb_rs/$total)*100);}
        $courb1=$courb2=$courb3=$courb4='[';
        //80, 100, 50, 50, 0, 60, 60, 100, 80
        for ($i=0; $i <10 ; $i++) { 
            # code...
            if($i<9){
                $courb1.=rand(0,100).',';
                $courb2.=rand(0,100).',';
                $courb3.=rand(0,100).',';
                $courb4.=rand(0,100).',';
            }else{
                $courb1.=rand(0,100);
                $courb2.=rand(0,100);
                $courb3.=rand(0,100);
                $courb4.=rand(0,100);
            }
            
        }
        $courb1.=']';
        $courb2.=']';
        $courb3.=']';
        $courb4.=']';
        //dd($courb1);
        return view('admin.index',compact('page','nb_actu','nb_g_image','nb_message','nb_rs','courb1','courb2','courb3','courb4','taux_fichiers','taux_image','taux_actu','taux_msg'));
    }
    public function index2()
    {  // dd('Admin');
        return view('admin.index2');
    }
    public function profil($pseudo)
    {        
        $compte=User::where('pseudo',$pseudo)->first();
        if(!$compte){
            abort(404);
        };
        $titre=$compte->prenom." ".$compte->nom;
    return view('admin.pages.profil',compact('titre','compte'));
    }
    public function changePassword($pseudo)
    {        
        $compte=User::where('pseudo',$pseudo)->first();
        if(!$compte){
            abort(404);
        };
        $titre=$compte->prenom." ".$compte->nom;
        return view('admin.pages.change-password',compact('titre','compte'));
    } 
    public function updatePassword(Request $request, $pseudo)
    {
       
         $this->validate($request, [
            'password' => 'required|string|min:6|max:100|confirmed',
            'lastpassword' => 'required|max:200',
        ]);
        $pcrypt=Hash::make($request->lastpassword);
        $upseudo=Auth::user()->pseudo;

         if($upseudo!=$pseudo){
            Session::flash('warning', 'Veuillez vérifier si vous êtes autorisé à effectuer cette opération  !');
            return redirect()->back();
        }
        
        if(!(Hash::check($request->lastpassword,Auth::user()->password))){
             Session::flash('warning', "Votre mot de passe ne correspond pas à l'ancien !");
            return redirect()->back();
        }
        $user = User::where('pseudo',$pseudo)->first();
        if(empty($user)){
            Session::flash('warning', 'Veuillez vérifier l\'ancien mot de passe  !');
            return redirect()->back();
        } 
        $user = User::where('pseudo',$pseudo)->first();
        $user->fill([
            'password' => Hash::make($request->password)
        ])->save();
        Session::flash('success', 'Mot de passe mise à jour avec succès !');
            return redirect(route('profil',$pseudo));
    }
    public function EditProfil($pseudo)
    {        
        $compte=User::where('pseudo',$pseudo)->first();
        if(!$compte){
            abort(404);
        };
        $titre=$compte->prenom." ".$compte->nom;
    return view('admin.pages.edit-profil',compact('titre','compte'));
    }
    public function postEditProfil(Request $request,$pseudo)
    {  
        $this->validate($request, [
            'nom' => 'required|min:2|max:200',
            'prenom' => 'required|min:2|max:200',
            'password' => 'required|min:6|max:100',
            'email' => 'required|email',
        ]);
        $requestData = $request->all();
       // dd($requestData);

         $user=Auth::user();
        $compte=User::where('pseudo',$pseudo)->first();
        
        if(!$compte || $user->pseudo!=$compte->pseudo){
            abort(404);
        };
        if(!(Hash::check($request->password, Auth::user()->password))){
             Session::flash('warning', "Votre mot de passe ne correspond pas à l'ancien !");
            return redirect()->back();
        }
        $compte->update([
            'nom'=>$requestData['nom'],
            'prenom'=>$requestData['prenom'],
            'email'=>$requestData['email'],
        ]);
        Session::flash('success', 'Votre compte à été mis à jour avec succès !');
        return redirect()->back();
    }
          public function postprofil(Request $request,$pseudo)
    {  
        $this->validate($request, [
            //'photo' => 'image',
            //'couverture' => 'image',
            'poste' => 'max:250',
            'entreprise' => 'max:100',
        ]);
         
        $requestData = $request->all();
        //dd('ss');
        //dd($requestData);
        
        $url="uploads/avatars/";
        if ($request->hasFile('photo')) {

            if(!is_dir($url)){
                @mkdir($url);
            }
            if($file=$request['photo'] ){
                $uploadPath = ($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                if($file->move($uploadPath, $fileName))
                $requestData['image'] = $url.$fileName;
                $requestData['photo']='';
            }
        }
        $url="uploads/couverture/";
        if ($request->hasFile('couverture')) {
            //dd('');
            if(!is_dir($url)){
                @mkdir($url);
            }

            if($file=$request['couverture'] ){
                $uploadPath = ($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                if($file->move($uploadPath, $fileName))
                $requestData['couverture'] = $url.$fileName;
            }
        }
       // dd($requestData);
        $user=Auth::user();
        $compte=User::where('pseudo',$pseudo)->first();
        
        if(!$compte || $user->pseudo!=$compte->pseudo){
            abort(404);
        };
        

        $compte->update($requestData);
         Session::flash('success', 'Profil mis à jour avec succès !');
        return redirect()->back();
    }
    public function editPassword()
    {        
        return view('admin.pages.profil');
    }

    public function getStat()
    {        
        return view('Admin.pages.stat');
    }
    public function getBilan()
    {        
        return view('Admin.pages.bilan');
    }
    public function getParam()
    {        
        return view('Admin.pages.param');
    }

    public function getSfdStat()
    {        
        return view('Admin.pages.sfd-stat');
    }
    public function getSfdBilan()
    {        
        return view('Admin.pages.sfd-bilan');
    }
    public function getSfdParam()
    {        
        return view('Admin.pages.sfd-param');
    }

    public function admin_credential_rules(array $data)
    {
        $messages = [
        'current-password.required' => 'Please enter current password',
        'password.required' => 'Please enter password',
        ];
        $validator = Validator::make($data, [
        'current-password' => 'required',
        'password' => 'required|same:password',
        'password_confirmation' => 'required|same:password',     
        ], $messages);
    
    return $validator;
    }  
    public function getEditPassword($slug)
    { 
        return view('web.edit-password');
    }
    public function postEditPassword(Request $request,$slug)
    { 
        if(!Auth::user()){abort(404);}
        $user=Auth::user();
        if($user->profile==$slug)
        {
            
            $request_data = $request->All();
            
            $validator = $this->validate($request, [
                'current-password' => 'required',
                'password' => 'required|same:password',
                'password_confirmation' => 'required|same:password',
            ]); 
                $current_password = Auth::User()->password;           
                if(Hash::check($request_data['current-password'], $current_password))
                {           
                    $user_id = Auth::User()->id;                       
                    $obj_user = User::find($user_id);
                    $obj_user->password = Hash::make($request_data['password']);;
                    $obj_user->save(); 
                    Session::flash('success', "Votre mot de passe a été modifié avec succès !");
                }
                else
                {           
                    Session::flash('danger', "Le mot de passe actuel n'est pas valide !");
                    $error = array('current-password' => 'Please enter correct current password');
                    return redirect()->back();   
                }
            
        }else
        {
            abort(503);
        }
        return redirect()->to(route('profile',$user->profile));
    }

        public function getEditProfil($slug)
    { 
        if(!Auth::user()){            
            abort(404);
        }
        $userOnline=Auth::user();
        $user=User::where("profile",$slug)->firstOrFail();
        /* Si c'est l'utilisateur qui est en ligne ou l'Admin*/
        if($user->profile==Auth::user()->profile || $user->is_admin){
            return view('web.edit-profile',compact('user'));
        }
        abort(404);  
    }
    public function getEditBiographie($slug)
    {
        if(!Auth::user()){            
            abort(404);
        }
        $userOnline=Auth::user();
        $user=User::where("profile",$slug)->firstOrFail();
        /* Si c'est l'utilisateur qui est en ligne ou l'Admin*/
        if($user->profile==Auth::user()->profile || $user->is_admin){
            return view('web.edit-biographie',compact('user'));
        }
        abort(404);        
    }
     public function getActivataion($slug){
        $user=User::where("profile",$slug)->firstOrFail();
        if(Auth::user() && Auth::user()->profile!=$slug ){

            Session::flash('warning', "OPERATION NON AUTORISE.<br>Vous n'avez pas l'autorisation requise !");
            return  redirect()->back();
        }
        if($user->etat==1){
             Session::flash('flash_message', "OPERATION NON AUTORISE.<br>Votre compte est déjà activé !");
            return  redirect()->back();
        }
        //dd(Auth::user()->profile);
        return view('web.activation-compte',compact('user'));

     }
      public function pActivataion($slug,Request $request){
        $user=User::where("profile",$slug)->firstOrFail();
       // dd($user);
        if(Auth::user() && Auth::user()->profile!=$slug ){
            Session::flash('warning', "OPERATION NON AUTORISE.<br>Vous n'avez pas l'autorisation requise !");
            return  redirect()->back();
        }
        if($user->etat==1){
             Session::flash('flash_message', "OPERATION NON AUTORISE.<br>Votre compte est déjà activé !");
            return  redirect()->back();
        }
        $requestData = $request->all();
        $scode=trim($requestData['active_code']);
       // dd($scode." ".$user->pcode);
        if($user->pcode==$scode){
                
                
                $user->etat=1;
                $user->update(['etat',1]);
                
                return  redirect(route("profile",$user->profile));
        }else{
           Session::flash('danger', "Le code d'activation n'est pas valide!");
            return  redirect()->back();
        }

      }
    public function postValidate($id,Request $request){
        if(!Auth::user() && Auth::user()->is_admin){            
            abort(404);
        }
        $user=User::where("id",$id)->firstOrFail();

        $requestData = $request->all();
        if(isset($requestData['action']) && $requestData['action']=="_act"){
                //dd($requestData);
                $user->update(['etat'=>1]);
                Session::flash('flash_message', 'Compte Activé avec succès !');
        }elseif(isset($requestData['action']) && $requestData['action']=="_dest"){
            # code...
            $newcode=str_random(10);
            //dd($newcode);
             $user->update(['etat'=>0,'pcode'=>$newcode]);
             Session::flash('flash_message', 'Compte désactivé avec succès !');
        }
       return  redirect()->back();

    }
    public function pEditProfil($slug,Request $request)
    {
        if(!Auth::user()){            
            abort(404);
        }

        $userOnline=Auth::user();
        $user=User::where("profile",$slug)->firstOrFail();
        /* Si c'est l'utilisateur qui est en ligne ou l'Admin*/
        if($user->profile==Auth::user()->profile || $user->is_admin){

            $this->validate($request, [
            'nom' => 'min:3|max:150',
            'prenom' => 'min:3|max:150',
            'adresse' => 'max:250',
            //'sexe' => 'required',
            //'date_nais' => 'required',
            'telephone' => 'max:20',
            'titre_id' => 'required',
            'universite_id' => 'required',
            'affiliation_id' => 'required',
            'photo' => 'image',
        ]);
        $requestData = $request->all();
        

        if ($request->hasFile('photo')) {
            if( $file=$request['photo']){
                $urlimg='/uploads/users/photos/';
                $uploadPath = ($urlimg);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['photo']= $urlimg.$fileName;
//dd('OK');
            }
        }else{
            $requestData['photo']='';
        } 
         $userNew=[
                    'nom' => $requestData['nom'],
                    'prenom' => $requestData['prenom'],
                    'sexe' => $requestData['sexe'],
                    'photo' => $requestData['photo'],
                   // 'profile' => $data['profile'],
                    //'pcode' => str_random(10),
                    //'date_nais' => $requestData['date_nais'],
                    'adresse' => $requestData['adresse'],
                    'telephone' => $requestData['telephone'],
                    'titre_id' => $requestData['titre_id'],
                    'universite_id' => $requestData['universite_id'],
                    'affiliation_id' => $requestData['affiliation_id']
        ];
       //dd($userNew);
       
        $user->update($userNew);

        Session::flash('success', 'La Mise à jour de votre profile a été effectuée  !');

        return redirect()->back();
        return redirect(route('profile',$user->profile));
        }
       // dd($request);
    }
    public function pPubValidate($id,Request $request)
    {
         if(!Auth::user() && Auth::user()->is_admin){            
            abort(404);
        }
        $pub=Article::where("id",$id)->firstOrFail();
       
        $requestData = $request->all();
        if(isset($requestData['action']) && $requestData['action']=="_pact"){
                //dd($requestData);
                $pub->update(['etat'=>1]);
                Session::flash('success', 'Publication activé avec succès !');
        }elseif(isset($requestData['action']) && $requestData['action']=="_pdst"){
            
             $pub->update(['etat'=>0]);
             Session::flash('warning', 'Publication désactivé avec succès !');
        }
       return  redirect()->back();

    }
    public function postEditBio($slug,Request $request)
    {
       
        if(!Auth::user()){            
            abort(404);
        }
        $userOnline=Auth::user();
        $user=User::where("profile",$slug)->firstOrFail();
        /* Si c'est l'utilisateur qui est en ligne ou l'Admin*/
        if($user->profile==Auth::user()->profile || $user->is_admin){
             $this->validate($request, [
            'bio' => 'min:10|string',
            ]);
              $requestData = $request->all();
             $bio=isset($requestData['bio'])?$requestData['bio']:'';
             $user->biographie=$bio;
             $user->save();
             return redirect(route('profile',$user->profile));
           // dd($bio);
        }
        
    }

    
}
