<?php

namespace App\Http\Middleware;

use Closure;
use \App\User ;
use \App\Models\Sfd;
use Cookie;
use Auth;
use View;

class SfdMiddleware 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

         $pid=(int)$request->sfd_id;
        if(empty($pid)){
            $pid=Cookie::get('sfd_id');
        }
        $sfd=Sfd::findOrFail($pid);
        
        //Session::flash('line', 'La Mise à jour de "Exercice" a été effectuée  !');
        $c=Cookie::queue('sfd_id', $pid,0);
        //dd($c);
        if(empty($sfd)){
            abort(404);
        }
        View::share(compact('sfd','type_pubs','sfd_id'));       
        return $next($request);
    }
}
