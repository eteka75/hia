<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use \App\User;

class DataHelper extends Model
{
    /**
     * Convert any string to a color code.
     *
     * @param string $string
     *
     * @return string
     */
    public static function stringToColorCode($string)
    {
        $code = dechex(crc32($string));

        return substr($code, 0, 6);
    }
    public static function TextColor($color)
    {
         $textColor='';
        for($i=0;$i<strlen($color);$i++){
            if(is_numeric($color[$i])){
                $textColor.=8-$color[$i];
            }else{
                 $textColor.='A';
            }
        }
        return $textColor;
    }

    /**
     * User link.
     *
     * @param mixed $user
     *
     * @return string
     */
    public static function userLink($user)
    {
        $url = config('chatter.user.relative_url_to_profile', '');

        if ('' === $url) {
            return '#_';
        }

        return static::replaceUrlParameter($url, $user);
    }

    /**
     * Replace url parameter.
     *
     * @param string $url
     * @param mixed  $source
     *
     * @return string
     */
    private static function replaceUrlParameter($url, $source)
    {
        $parameter = static::urlParameter($url);

        return str_replace('{'.$parameter.'}', $source[$parameter], $url);
    }

    /**
     * Url parameter.
     *
     * @param string $url
     *
     * @return string
     */
    private static function urlParameter($url)
    {
        $start = strpos($url, '{') + 1;

        $length = strpos($url, '}') - $start;

        return substr($url, $start, $length);
    }
    /**
     * Url parameter.
     *
     * @param string $nom, $prenom
     *
     * @return string
     */
    
    public static function generatePseudo($n,$p=""){
       $n1=substr($n,0,15);
       $pr1=substr($p,0,15);
        $i=0;
        do {

            if($i>0){
             $pseudo=str_slug($n1." ".$pr1,'.').''.$i;
            }else{
                $pseudo=str_slug($n." ".$p,'.'); 
            }
           
            $nb=User::Where('pseudo',$pseudo)->count();
            $i++;
        }while($nb>0);
        return $pseudo;
    }
    /**
     * This function will demote H1 to H2, H2 to H3, H4 to H5, etc.
     * this will help with SEO so there are not multiple H1 tags
     * on the same page.
     *
     * @param HTML string
     *
     * @return HTML string
     */
    public static function demoteHtmlHeaderTags($html)
    {
        $originalHeaderTags = [];
        $demotedHeaderTags = [];

        foreach (range(100, 1) as $index) {
            $originalHeaderTags[] = '<h'.$index.'>';

            $originalHeaderTags[] = '</h'.$index.'>';

            $demotedHeaderTags[] = '<h'.($index + 1).'>';

            $demotedHeaderTags[] = '</h'.($index + 1).'>';
        }

        return str_ireplace($originalHeaderTags, $demotedHeaderTags, $html);
    }
    public static function dateToMysql($date, $f='dd/mm/yyyyy')
    {
        if(strlen($date)==10){
            $exp=explode("/",$date);
            if(count($exp)==3){
                $date=$exp[2].'-'.$exp[1]."-".$exp[0];
            }
        }
        return $date;
    } 
    public static function getPositionDate($date,$position=1, $f='y-m-d')
    {
        $param='-';
        if(strlen($date)==10){
            $exp=explode("-",$date);
            if(count($exp)==3){
                $param=isset($exp[$position-1])?$exp[$position-1]:'-';
            }
        }
        return (int)$param;
    } 
    public static function getTabAnneActivite($date_debut,$duree)
    {
        
        $annee_debut=Self::getPositionDate($date_debut,1);//2019 |1-8
        $mois_debut=Self::getPositionDate($date_debut,2);
        $TabDate=[$annee_debut=>$annee_debut];
        for ($i=0; $i < $duree ; $i++) { 
            $total_mois=$mois_debut++;
            if($total_mois==12){
                $annee_debut++;
                array_push($TabDate, $annee_debut);
                $mois_debut=1;
            }
        }
        return $TabDate;
    } 
    public static function getTabIntActivite($date_debut,$duree,$annee)
    {

        $a_debut=$annee_debut=Self::getPositionDate($date_debut,1);//2019 |1-8
        $m_debut=$mois_debut=Self::getPositionDate($date_debut,2);
        $TabDateInt=[];
        $j=$k=0;
        for ($i=0; $i < $duree ; $i++) { 
             $total_mois=$mois_debut++;
            if($total_mois==12){
                $annee_debut++;
                $annee_debut;
                $mois_debut=1;
            }else{
                 $annee_debut;
            }
           
            $j++;
            if($a_debut!=$annee){$y=1;$z=0;}else{$z=$y=$m_debut;}
            //echo ($a_debut)."===";
            if($annee==$annee_debut){
                $k++;
            $nb_rm=($m_debut+$j)-12;
            
             $m_debut."+".$j."-12=".$nb_rm."<br>";
            $nbr=($nb_rm>0)?$nb_rm:0;
            $nbm_fin=$z+$k;
            //echo $y."+".$k."=".$nbm_fin."<hr>";
             //if($nbm_fin>12){$nbm_fin=12;}
            $TabDateInt[$annee]=["annee"=>$annee,'mois_debut'=>$y,'mois_fin'=>$nbm_fin];
            }else{
               // $k=1;
            }
           // }else{

            //}
        }        
        return $TabDateInt;

    }
    public static function dateToInput($date, $f='dd/mm/yyyyy')
    {
        if(strlen($date)==10){
            $exp=explode("-",$date);
            if(count($exp)==3){
                $date=$exp[2].'/'.$exp[1]."/".$exp[0];
            }
        }
        return $date;
    }
    public static function dateEnFr($date, $format='',$sortie='court') 
    {
       $tmoisC=[1=>"Jan.",2=>'Fév.',3=>'Mar.',4=>'Avr.',5=>'Mai',6=>'Juin',7=>'Juil.',8=>'Août',9=>'Sep.',10=>'Oct.',11=>'Nov.',12=>'Déc.'];
        $tmoisL=[1=>"Janvier",2=>'Février',3=>'Mars',4=>'Avril',5=>'Mai',6=>'Juin',7=>'Juillet',8=>'Août',9=>'Septembre',10=>'Octocbre',11=>'Novembre',12=>'Décembre'];
      if(strlen($date)==10){
            $exp=explode("-",$date);
            $mois=$exp[1];
            if($sortie=='court'){
                $mois=isset($tmoisC[$exp[1]]);
            }else{
                $mois=isset($tmoisL[$exp[1]]);
            }
            
            if(count($exp)==3){
                $date=$exp[2].' '.$mois.",".$exp[0];
            }
        }  
        return $date;
    
    }
    public static function dateToFr($date, $format='') 
    {
        $english_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
        $french_days = array('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche');
        $english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        $french_months = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
        return str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, strtotime($date) ) ) );
    }
}
