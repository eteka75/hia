<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Promoteur;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
      use Notifiable, HasRoles;
         /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom','prenom','image','couverture','poste','entreprise','id_if','email','type', 'password','pseudo','etat','email_verified_at','etat','promoteur_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $appends = ['user_ids'];
    /* public function getInstIdsAttributes()
    {
        return $this->InstitutionsAdministres->pluck('id_institution');
    }
    */
    public function InstitutionsAdministres()
    {
        return $this->belongsToMany('App\Models\IFinanciere','institution_user','id_user', 'id_institution')->withTimestamps();
    }
    
    public function isAdmin(){
        return $this->type=='ADMIN'?TRUE:FALSE;
    }
    public function isInstitution(){
      //dd($this->type);
        return $this->type=='SFD' && $this->InstitutionsAdministres ?TRUE:FALSE;
    }
    public function type(){
   
    switch ($this->type) {
      case 'SFD':
        $type="Institution financière";
        break;
      case 'PME':
        $type="Promoteur";
        break;
      case 'ADMIN':
        $type="Administrateur";
        break;
      
      default:
        $type="Compte par défaut";
        break;
    }
       return $type;
    }
    public function promoteur()
    {
        return $this->hasOne("App\Models\Promoteur");
    }

    public function getSexe() {
        $sexe='-';
      if($this->sexe=='H'){
        $sexe='Masculin';
    }elseif($sexe=='F'){
       $sexe='Féminin';
    }

       return$sexe;
    }
    public function isPME() {
      return $this->type=="PME"?true:false;
    }
    public function isSFD() {
      return $this->type=="SFD"?true:false;
    }
    public function groupement()
    {
        return $this->hasOne('App\Models\Groupement');
    }
    public function titre() {
      return $this->belongsTo('App\Models\Titre', "titre_id");
    }
    public function publications()
    {
        return  $this->belongsToMany('App\Models\Publication', 'publication_user','user_id', 'publication_id')->withTimestamps();
    }

    public function dossierCredit()
    {
        return  $this->hasMany('App\Models\DossierCredit',  'id_user');
    }

    public function biens()
    {
      return $this->hasMany('App\Models\Bien');
    }

    public function activites()
    {
      return $this->hasMany('App\Models\Activite');
    }
    public function immobilisations()
    {
      return $this->hasMany('App\Models\Immobilisation');
    }
    public function finances()
    {
      return $this->hasMany('App\Models\Finance');
    }
    public function menages()
    {
      return $this->hasMany('App\Models\Menage');
    }

    public function entreprise()
    {
        return $this->hasOne('App\Models\Entreprise');
    }

    public function terre()
    {
        return $this->hasOne('App\Models\Terre');
    }

     //RELATIONSHIPS FOR EBST INVESTMENT

    public function investment_biens() 
    {
      return $this->hasMany('App\InvestmentBien');  
    }

    public function investment_activites() 
    {
      return $this->hasMany('App\InvestmentActivite');  
    }
    public function investment_immobilisations() 
    {
      return $this->hasMany('App\InvestmentImmobilisation');  
    }
    public function investment_finances() 
    {
      return $this->hasMany('App\InvestmentFinance');  
    }
    public function investment_menages() 
    {
      return $this->hasMany('App\InvestmentMenage');  
    }
      
    public function investment_entreprise()
    {
        return $this->hasOne('App\InvestmentEntreprise');
    }
      
    public function investment_terre()
    {
        return $this->hasOne('App\InvestmentTerre');
    }
}
