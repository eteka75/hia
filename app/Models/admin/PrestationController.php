<?php
#ETEKA Modification
namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Prestation;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;

class PrestationController extends Controller
{
    protected $page="prestation";
    public function __construct()
    {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $perPage = 10;
        $uid=Auth::user()?Auth::user()->id:0;

        if (!empty($keyword)) {
            $prestation = Prestation::orderBy('categorie')->where('titre', 'LIKE', "%$keyword%")
				->orWhere('slug', 'LIKE', "%$keyword%")
				->orWhere('image', 'LIKE', "%$keyword%")
				->orWhere('contenu', 'LIKE', "%$keyword%")
				->orWhere('id_user', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $prestation = Prestation::paginate($perPage);
        }
        $page=$this->page;
        return view('admin.prestation.index', compact('prestation','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $replace="/create";
        $page=$this->page;
        return view('admin.prestation.create',compact('replace','page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
            'image' => 'image',
			'contenu' => 'required|min:5'
		]);
        $requestData = $request->all();
        $lml=str_slug($requestData["titre"]);
        $requestData["slug"]=substr($lml,0,250); 
        
  $url='/uploads/pages/';
        if ($request->hasFile('image')) {
            if($file=$request['image'] ){
                $uploadPath = public_path($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['image'] = $url.$fileName;
            }else{
                $requestData['image']="";
            }
        }else{
                $requestData['image']="";
            }
       

        $did=Auth::user()?Auth::user()->id:0;
        $requestData["id_user"]=$did;
        
        Prestation::create($requestData);

        Session::flash('success', 'Prestation ajouté avec succès !');

        return redirect('admin/prestation');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $prestation = Prestation::findOrFail($id);
        $replace="/".$id;
        $page=$this->page;
        return view('admin.prestation.show', compact('prestation','replace','page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $prestation = Prestation::findOrFail($id);
        $replace="/".$id."/edit";
        $page=$this->page;
        return view('admin.prestation.edit', compact('prestation','replace','page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
       $this->validate($request, [
            'titre' => 'required',
            'image' => 'image',
            'contenu' => 'required|min:5'
        ]);
        $requestData = $request->all();
        $lml=str_slug($requestData["titre"]);
        $requestData["slug"]=substr($lml,0,250); 
        
  $url='/uploads/pages/';
        if ($request->hasFile('image')) {
            if($file=$request['image'] ){
                $uploadPath = public_path($url);

                $extension = $file->getClientOriginalExtension();
                $fileName = rand(11111, 99999) . '.' . $extension;

                $file->move($uploadPath, $fileName);
                $requestData['image'] = $url.$fileName;
            }else{
                $requestData['image']="";
            }
        }else{
                $requestData['image']="";
            }
       
         $prestation = Prestation::findOrFail($id);
        if($requestData['image']==""){
            $requestData['image']=$prestation->image;
        }
        $prestation->update($requestData);

        Session::flash('info', 'La Mise à jour de la "Prestation" a été effectuée  !');

        return redirect('admin/prestation');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Prestation::destroy($id);

        Session::flash('danger', 'La suppression de "Prestation" a été effectuée !');

        return redirect('admin/prestation');
    }
}
