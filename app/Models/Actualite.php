<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Actualite extends Model
{
     use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'actualites';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
        protected $dates = ['deleted_at'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['titre', 'slug', 'chapeau', 'photo', 'categorie_id', 'corps', 'etat','id_user'];

    public function categorie()
    {
      return $this->belongsTo('App\Models\Categorie','categorie_id');
    }
    
}
