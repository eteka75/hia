@extends('layouts.web')
@section('title','Affichage de Publication')
@section('content')
    <div class="container">
             <div class="row">
                <div class="col-sm-12 ">
                    <ol class="breadcrumb mb30">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Accueil </a></li>
                        <li class="breadcrumb-item"><a href="{{url('/administration')}}">Administration </a></li>
                        <li class="breadcrumb-item"> <a href="{{ url('/administration/publication/') }}">Publications</a></li>
                        <li class="breadcrumb-item"> Publication #{{ $publication->id }} </li>
                    </ol>
                </div>
            </div>
            <div class="row">
            
                    @include('admin.sidebar')

                    <div class="col-md-10">
                        <div class="content_main rond3">
                            <div class="panel panel-default">
                                <div class="panel-heading">Publication #{{ $publication->id }}</div>
                                <div class="panel-body">

                                    <a href="{{ url('/administration/publication') }}" title="Back"><button class="btn btn- btn-xs bg-white text-primary borderc mt20"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>

                                   <!--div>
                                    <a href="{{ url('/administration/publication/' . $publication->id . '/edit') }}" title="Edit Publication"><button class="btn btn-primary btn-xs mt20"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editer</button></a>
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['administration/publication', $publication->id],
                                        'style' => 'display:inline'
                                    ]) !!}
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs mt20',
                                                'title' => 'Suppression Publication',
                                                'onclick'=>'return confirm("Voulez vous vraiment supprimer cet élément ?")'
                                        ))!!}
                                    {!! Form::close() !!}
                                    <div-->
                                    <br/>
                                    <br/>

                                    <div class="table-responsive">
                                        <table class="table table-striped table-borderless">
                                            <tbody>
                                                <!--tr>
                                                    <th>CODE</th><td>{{ $publication->code }}</td>
                                                </tr-->
                                                <tr>
                                                    <th width="30%"> {{ trans('publication.titre') }} </th>
                                                    <td> {{ $publication->titre }} </td>
                                                </tr>
                                                <tr>
                                                    <th> {{ trans('publication.annee_pub') }} </th>
                                                    <td> {{ $publication->annee_pub }} </td>
                                                </tr>
                                                <tr>
                                                    <th> {{ trans('publication.volume') }} </th>
                                                    <td> {{ $publication->volume }} </td>
                                                </tr>
                                                <tr>
                                                    <th> {{ trans('publication.numero') }} </th>
                                                    <td> {{ $publication->numero }} </td>
                                                </tr>
                                                <tr>
                                                    <th> {{ trans('publication.discipline_id') }} </th>
                                                    <td> {{ $publication->discipline?$publication->discipline->nom:'' }} </td>
                                                </tr>
                                                <tr>
                                                    <th> {{ trans('publication.journal') }} </th>
                                                    <td> {{ $publication->journal }} </td>
                                                </tr>
                                               
                                                <tr>
                                                    <th> {{ trans('publication.impactfactor') }} </th>
                                                    <td> {{ $publication->impactfactor }} </td>
                                                </tr>
                                                
                                                <tr>
                                                    <th> {{ trans('publication.resume') }} </th>
                                                    <td> {{ $publication->resume }} </td>
                                                </tr>
                                                
                                                <tr>
                                                    <th> {{ trans('publication.mots_cle') }} </th>
                                                    <td> {{ $publication->mots_cle }} </td>
                                                </tr>
                                                
                                                <tr>
                                                    <th> {{ trans('publication.type_document_id') }} </th>
                                                    <td> {{ $publication->type?$publication->type->nom:'' }} </td>
                                                </tr>
                                                @if($publication->url_fichier)
                                                <tr>
                                                    <th> {{ trans('publication.url_fichier') }} </th>
                                                    <td> <a href="{{asset($publication->url_fichier)}}"><img src="{{asset('assets/images/pdf.png')}}" class="pst10pb10ml10" alt="PDF" height="20px"> Voir le fichier de la publication</a> </td>
                                                </tr>
                                                @endif
                                                <!--tr>
                                                    <th> {{ trans('publication.image_pub') }} </th>
                                                    <td> {{ $publication->image_pub }} </td>
                                                </tr-->
                                                
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                     </div>
            </div>
        </div>
    </div>
@endsection
