<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePassationMarchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passation_marches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titre');
            $table->string('slug')->nullable();
            $table->string('type')->nullable();
            $table->string('fichier');
            $table->longText('contenu')->nullable();
            $table->integer('id_user')->nullable();
            $table->index(['id']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('passation_marches');
    }
}
